This page tracks all changes since the last release of VG.

When you merge a PR, add a bullet point to this page.

When you do a release, copy the changelog from this page and clear it out.

# Changes For Next Release

* `vg construct` now has a `-A, --alt-paths-plain` option for storing IDs from the VCF instead of hash-based IDs for alt allele paths.
* `vg call` patched so that certain problem cases no longer take forever.
* Mac CI now actually installs Node

## Updated Submodules

## New Submodules

## Removed submodules



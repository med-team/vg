Welcome to the `vg` wiki!

**In a hurry? Check our [[Quickstart]] guide.**

Variation graphs are powerful objects capable of describing populations of genomes. `vg` provides a set of tools to construct, manipulate, and visualize them in the context of genome informatics.

Please feel free to edit and extend this wiki! For any questions or concerns please use the issues page in this repository, or drop by [the vg chat on gitter](https://gitter.im/vgteam/vg) or irc (#vg in freenode).

## First things first

* [[Building VG (or not building vg)]]  
* [[Building VG on Cent OS 6.6 or 7  and using it]]

## VG's formats and some basic usage

* [[File Formats]]
* [[Basic Operations]]
* [[Considerations for Batch Processing]]
* [[Programming with the vg API]]
* [[Path Metadata Model]]

## Graph / index construction

* [[Graph construction from a reference and a VCF file|Construction]]
* [[Index types|Index Types]]
* [[Automatic index construction|Automatic indexing for read mapping and downstream inference]]
* [[Manual index construction|Index Construction]]
* [[Building and manipulating GBWTs with vg gbwt|VG GBWT Subcommand]]
* [[Dealing with huge datasets|Indexing Huge Datasets]]

## Read alignment

* `vg giraffe`: [[Mapping short reads with Giraffe]]
* `vg giraffe`: [[Giraffe best practices]]
* `vg giraffe`: [[Mapping to a personalized reference|Haplotype Sampling]]
* `vg mpmap`: [[Multipath alignments and vg mpmap]]
* `vg map`: [[Working with a whole genome variation graph]]

## Other VG tools

* [[Visualization]]
* [[SV genotyping and variant calling]]
* [[Long read assemblies using vg msga]]
* [[Simulating reads with vg sim]]
* [[Transcriptomic analyses]]
* [[Changing references]]
* [[Extracting a FASTA from a Graph]]

## VG Project Management

* [[Releases]]
* [[Roadmap]]
* [[Draft Changelog]]
* [[Testable Documentation]]

## VG RDF

* [[RDF:-for-VG]]
* [[Example:-Serving-out-an-RDF-version-of-a-VG-graph-with-Apache-Fuseki]]
* [[Example: Serving out an RDF version of a VG graph with Blazegraph]]
* [[Annotating-a-VG-graph-the-RDF-way]]
* [2 ecoli genomes, with ensembl and uniprot annotation](/vgteam/vg/wiki/VG-RDF,-the-Ensembl-bacteria-E.-coli-genome-hack-attack)